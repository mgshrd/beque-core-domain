Require Import intf.array.ElemHandle.

Module UnitHandle <: ElemHandle.

Definition handle := unit.
Definition eq_handle_dec :
  forall h1 h2 : handle, {h1 = h2} + {h1 <> h2}.
Proof.
  decide equality.
Defined.

Definition h := tt.

End UnitHandle.
