Require Import opt_utils.

Require Import IO.intf.World.

Require Import LTL.util.LTL.

Module LTLFrameUtil
       (w : World)
.

Module ltl := LTL w.
Export ltl.
Import ltl.

(** definition of [tillnow] interpreted in a framed world, i.e any unmapped message terminates the state. *)
Definition instate
           (starts : list w.msg -> Prop)
           (propagates : list w.msg -> Prop)
           (ends : list w.msg -> Prop) :
  list w.msg -> Prop
  := tillnow starts (fun t => propagates t /\ ~ends t).

Definition instate_msg
           (ms me : w.msg)
           (mp : w.msg -> Prop) :
  list w.msg -> Prop
  := let hd_error := fun t =>
                       match t with
                         | nil => None
                         | (h::_)%list => Some h
                       end
     in
     instate
       (fun t => hd_error t = Some ms)
       (fun t => optpred mp (hd_error t))
       (fun t => hd_error t = Some me).

End LTLFrameUtil.
