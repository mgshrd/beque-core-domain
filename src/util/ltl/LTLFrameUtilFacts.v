Require Import ltac_utils.

Require Import IO.intf.World.

Require Import Domain.

Require Import LTLFrameUtil.

Module LTLFrameUtilFacts
       (w : World)
.

Module ltlfu := LTLFrameUtil w.
Import ltlfu.

Lemma instate_widen :
  forall (s k e s' k' e': list w.msg -> Prop) t,
    (forall t, s t -> s' t) ->
    (forall t, k t -> k' t) ->
    (forall t, e' t -> e t) ->
    instate s k e t ->
    instate s' k' e' t.
Proof.
  intros. induction t.
  {
    unfold instate in H2. unfold tillnow in H2.
    destruct H2 as [ sx [ t' [ ppf H2 ] ] ].
    nilapp ppf.
    destruct H2.
    assert (H3' := H3 nil (ex_intro _ nil eq_refl)). clear H3. destruct H3'.
    exists nil. exists nil.
    split; [ reflexivity | ].
    split; [ apply H; exact H2 | ].
    intros.
    destruct H5 as [ s'' H5 ].
    nilapp H5.
    split; [ apply H0; exact H3 | ].
    intro; elim H4.
    apply H1; exact H6.
  }

  unfold instate, tillnow.
  unfold instate, tillnow in H2.
  destruct H2 as [ sx [ t' [ ppf H2 ] ] ].
  exists sx. exists t'.
  split; [ exact ppf | ].
  destruct H2.
  split; [ apply H; exact H2 | ].
  intros.
  assert (H3' := H3 _ H4). clear H3.
  destruct H3'.
  split; [ apply H0; exact H3 | ].
  intro. elim H5.
  apply H1. exact H6.
Qed.

End LTLFrameUtilFacts.
