Require Import IO.intf.World.
Require Import IO.intf.IORaw.
Require Import IO.intf.IOAux.

(*Require Import interfaces.Domain.*)

Module ExistentialAction
       (w : World)
       (ior : IORaw w)
(* this is useful during domain implementation, so break the circular dependency here d : Domain w ior*)
.

Module io := IOAux w ior.

(** A type of action, that only guarantees that the segment after exectuion is its, all other postconditions must follow from that. *)
Definition plain_action
           {d_action : Set}
           (d_action_res : d_action -> Set)
           (d_segment_of_action_with_res : forall a, d_action_res a -> list w.msg -> Prop)
           pre a :=
  ior.IO
    pre
    (d_action_res a)
    (fun res s t => d_segment_of_action_with_res a res s).

End ExistentialAction.
