Require Import Util.DelayExtractionDefinitionToken.

Require Import IO.intf.World.
Require Import IO.intf.IORaw.

Require Import Domain.

(** A [Domain] with Ocaml runtime support. *)
Module Type DomainExtractOcaml
       (w : World)
       (ior : IORaw0 w)
<: Domain w ior
.

Module Type dtmp := Domain w ior.
Include dtmp.

Module extractOcaml (token : DelayExtractionDefinitionToken).
End extractOcaml.

End DomainExtractOcaml.
