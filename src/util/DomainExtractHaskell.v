Require Import Util.DelayExtractionDefinitionToken.

Require Import IO.intf.World.
Require Import IO.intf.IORaw.

Require Import Domain.

(** A [Domain] with Haskell runtime support. *)
Module Type DomainExtractHaskell
       (w : World)
       (ior : IORaw0 w)
<: Domain w ior
.

Module Type dtmp := Domain w ior.
Include dtmp.

Module extractHaskell (token : DelayExtractionDefinitionToken).
End extractHaskell.

End DomainExtractHaskell.
