Require Import sublist.
Require Import dec_utils.
Require Import ltac_utils.

Require Import IO.intf.World.
Require Import IO.intf.IORaw.
Require Import IO.intf.IOAux.

Require Import LTL.util.LTL.

Require Import Domain.

(** Domain of two independent domains. *)
Module Type TwoDomains
       (w : World)
       (ior : IORaw0 w)
       (d1 : Domain w ior)
       (d2 : Domain w ior)
<: Domain w ior
.

Module io := IOAux0 w ior.
Module ltl := LTL w.

(*************** Basic [Domain] data *******************)
(** The actions this domain defines. *)
#importashead Definition action
Proof.
  exact (d1.action + d2.action)%type.
Defined.

#importashead Definition eq_action_dec
Proof.
  assert (Hx := d1.eq_action_dec). assert (Hy := d2.eq_action_dec).
  decide equality.
Defined.

(** Specify the type of the result when [a] is executed *)
#importashead Definition action_res
Proof.
  intro.
  exact (match a with
         | inl a' => d1.action_res a'
         | inr a' => d2.action_res a'
         end).
Defined.

#importashead Definition eq_action_res_dec
Proof.
  intros.
  destruct a.
  - apply (d1.eq_action_res_dec a).
  - apply (d2.eq_action_res_dec a).
Defined.

(** The properties this domain defines. *)
#importashead Definition prop
Proof.
  exact (d1.prop + d2.prop)%type.
Defined.

#importashead Definition eq_prop_dec
Proof.
  assert (Hx := d1.eq_prop_dec). assert (Hy := d2.eq_prop_dec).
  decide equality.
Defined.


(*************** [Domain] action marker *******************)

(** Marker expressing that the segment of messages [s] was generated during performing action [a] which produced the value [res]. *)
#importashead Definition segment_of_action_with_res
Proof.
  intros. destruct a.
  - exact (d1.segment_of_action_with_res a res s).
  - exact (d2.segment_of_action_with_res a res s).
Defined.




(*************** [Domain] action and property interactions *******************)

Parameter d1_action_idempotent_for_d2_prop :
  forall (a : d1.action) (res : d1.action_res a) (p : d2.prop), Prop.
Parameter d1_action_idempotent_for_d2_prop_dec :
  forall (a : d1.action) (res : d1.action_res a) (p : d2.prop),
    { d1_action_idempotent_for_d2_prop a res p}+
    {~d1_action_idempotent_for_d2_prop a res p}.
Parameter d2_action_idempotent_for_d1_prop :
  forall (a : d2.action) (res : d2.action_res a) (p : d1.prop), Prop.
Parameter d2_action_idempotent_for_d1_prop_dec :
  forall (a : d2.action) (res : d2.action_res a) (p : d1.prop),
    { d2_action_idempotent_for_d1_prop a res p}+
    {~d2_action_idempotent_for_d1_prop a res p}.

(** Specify property propagations guaranteed by this domain.

If a trace [t] has property [p], executing action [a] which results in [res] and adds a segment of messages [s], does not interfere with property [p], that is trace [s ++ t] still has property [p].
*)
#importashead Definition propagates
Proof.
  intros. magic_case a; magic_case p.
  - subst a p; exact (d1.propagates a0 res p0).
  - subst a p; exact (d1_action_idempotent_for_d2_prop a0 res p0).
  - subst a p; exact (d2_action_idempotent_for_d1_prop a0 res p0).
  - subst a p; exact (d2.propagates a0 res p0).
Defined.

#importashead Definition propagates_dec
Proof.
  intros. destruct a, p.
  - apply d1.propagates_dec.
  - assert (Hx := d1_action_idempotent_for_d2_prop_dec). firstorder.
  - assert (Hx := d2_action_idempotent_for_d1_prop_dec). firstorder.
  - apply d2.propagates_dec.
Defined.

Parameter d1_action_back_idempotent_for_d2_prop :
  forall (a : d1.action) (res : d1.action_res a) (p : d2.prop), Prop.
Parameter d1_action_back_idempotent_for_d2_prop_dec :
  forall (a : d1.action) (res : d1.action_res a) (p : d2.prop),
    { d1_action_back_idempotent_for_d2_prop a res p}+
    {~d1_action_back_idempotent_for_d2_prop a res p}.
Parameter d2_action_back_idempotent_for_d1_prop :
  forall (a : d2.action) (res : d2.action_res a) (p : d1.prop), Prop.
Parameter d2_action_back_idempotent_for_d1_prop_dec :
  forall (a : d2.action) (res : d2.action_res a) (p : d1.prop),
    { d2_action_back_idempotent_for_d1_prop a res p}+
    {~d2_action_back_idempotent_for_d1_prop a res p}.
(** Specify backwards property propagations guaranteed by this domain.

If a trace [s++t] has property [p], where [s] is the segment of messages generated while executing action [a] which resulted in [res], then [p] already did hold on [t] as well.
*)
#importashead Definition back_propagates
Proof.
  intros. destruct a, p.
  - exact (d1.back_propagates a res p).
  - exact (d1_action_back_idempotent_for_d2_prop a res p).
  - exact (d2_action_back_idempotent_for_d1_prop a res p).
  - exact (d2.back_propagates a res p).
Defined.

#importashead Definition back_propagates_dec
Proof.
  intros. destruct a, p.
  - apply d1.back_propagates_dec.
  - apply d1_action_back_idempotent_for_d2_prop_dec.
  - apply d2_action_back_idempotent_for_d1_prop_dec.
  - apply d2.back_propagates_dec.
Defined.



(*************** [Domain] denotations *******************)


(** if a [prop] is closely linked to an [action] this helper creates trace property, where the action segment of [a] starts the valid range, and it propagates until [p] propagates *)

(** Translate property ids into trace properties. *)
#importashead Definition prop_to_trace_prop
Proof.
  intro.
  exact (match p with
         | inl p' => d1.prop_to_trace_prop p'
         | inr p' => d2.prop_to_trace_prop p'
         end).
Defined.

(** Translate an action id into a IO action *)
#importdefsupto action_to_IO
Definition action_to_IO
           (a : action)
  : { pre_post : _ & (ior.IO (fst pre_post) (action_res a) (snd pre_post) *
                      (forall res s t,
                          (snd pre_post) res s t ->
                          segment_of_action_with_res a res s))%type } :=
  match a with
  | inl a' => d1.action_to_IO a'
  | inr a' => d2.action_to_IO a'
  end.



(********************** [Domain] consistency *************************)
Conjecture d1_d2_propagates_correct :
  forall {p a res},
    propagates (inl a) res (inr p) ->
    forall {s},
      segment_of_action_with_res (inl a) res s ->
      forall {t},
        prop_to_trace_prop (inr p) t ->
        prop_to_trace_prop (inr p) (s ++ t).
Conjecture d2_d1_propagates_correct :
  forall {p a res},
    propagates (inr a) res (inl p) ->
    forall {s},
      segment_of_action_with_res (inr a) res s ->
      forall {t},
        prop_to_trace_prop (inl p) t ->
        prop_to_trace_prop (inl p) (s ++ t).

(** Consistency requirement between [prop_to_trace_prop], [segment_of_action_with_res], and [propagates]. *)
#importashead Lemma propagates_correct
Proof.
  intros ? ? ? proppf ? segpf ? oldpf.
  destruct p, a;
    [ exact (d1.propagates_correct proppf segpf oldpf)
    | exact (d2_d1_propagates_correct proppf segpf oldpf)
    | exact (d1_d2_propagates_correct proppf segpf oldpf)
    | exact (d2.propagates_correct proppf segpf oldpf)
    ].
Qed.

Conjecture d1_d2_back_propagates_correct :
  forall {p a res},
    back_propagates (inl a) res (inr p) ->
    forall {s},
      segment_of_action_with_res (inl a) res s ->
      forall {t},
        prop_to_trace_prop (inr p) (s ++ t) ->
        prop_to_trace_prop (inr p) t.
Conjecture d2_d1_back_propagates_correct :
  forall {p a res},
    back_propagates (inr a) res (inl p) ->
    forall {s},
      segment_of_action_with_res (inr a) res s ->
      forall {t},
        prop_to_trace_prop (inl p) (s ++ t) ->
        prop_to_trace_prop (inl p) t.

(** Consistency requirement between [prop_to_trace_prop], [segment_of_action_with_res], and [back_propagates]. *)
#importashead Lemma back_propagates_correct
Proof.
  intros ? ? ? proppf ? segpf ? oldpf.
  destruct p, a;
    [ exact (d1.back_propagates_correct proppf segpf oldpf)
    | exact (d2_d1_back_propagates_correct proppf segpf oldpf)
    | exact (d1_d2_back_propagates_correct proppf segpf oldpf)
    | exact (d2.back_propagates_correct proppf segpf oldpf)
    ].
Qed.

#importrestdefs

End TwoDomains.
