Require Import sublist.
Require Import dec_utils.
Require Import ltac_utils.

Require Import IO.intf.World.
Require Import IO.intf.IORaw.
Require Import IO.intf.IOAux.

Require Import LTL.util.LTL.

Require Import Domain.
Require Import TwoDomains.

(** Domain of two independent domains. *)
Module TwoIndependentDomains
       (w : World)
       (ior : IORaw0 w)
       (d1 : Domain w ior)
       (d2 : Domain w ior)
<: TwoDomains w ior d1 d2
.

Module io := IOAux0 w ior.
Module ltl := LTL w.



(*************** [Domain] action and property interactions *******************)

#importashead Definition d1_action_idempotent_for_d2_prop
Proof.
  intros. exact True.
Defined.

#importashead Definition d1_action_idempotent_for_d2_prop_dec
Proof.
  intros. left; exact I.
Defined.

#importashead Definition d2_action_idempotent_for_d1_prop
Proof.
  intros. exact True.
Defined.

#importashead Definition d2_action_idempotent_for_d1_prop_dec
Proof.
  intros. left; exact I.
Defined.

(** Specify property propagations guaranteed by this domain. *)

#importashead Definition d1_action_back_idempotent_for_d2_prop
Proof.
  intros. exact True.
Defined.

#importashead Definition d1_action_back_idempotent_for_d2_prop_dec
Proof.
  intros. left; exact I.
Defined.

#importashead Definition d2_action_back_idempotent_for_d1_prop
Proof.
  intros. exact True.
Defined.

#importashead Definition d2_action_back_idempotent_for_d1_prop_dec
Proof.
  intros. left; exact I.
Defined.


(********************** [Domain] consistency *************************)
#importashead Axiom d1_d2_propagates_correct
#importashead Axiom d2_d1_propagates_correct

(** Consistency requirement between [prop_to_trace_prop], [segment_of_action_with_res], and [propagates]. *)

#importashead Axiom d1_d2_back_propagates_correct
#importashead Axiom d2_d1_back_propagates_correct

(** Consistency requirement between [prop_to_trace_prop], [segment_of_action_with_res], and [back_propagates]. *)

#importrestdefs

End TwoIndependentDomains.
