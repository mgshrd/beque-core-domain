Require Import sublist.

Require Import IO.intf.World.
Require Import IO.intf.IORaw.

Require Import Domain.

Module Type DomainSegmentCanonicalProperties
       (w : World)
       (ior : IORaw0 w)
       (d : Domain w ior)
.

(********* Properties of action segments (as defined by [d.segment_of_action_with_res].) *********)
(** each action must do something *)
(* TODO too strong req *)
Parameter segment_not_empty :
  forall a res s,
    d.segment_of_action_with_res a res s ->
    s <> nil.

(** If a segment is a result of one action, it, or any of its prefix/postfixes cannot be a result of a different action. *)
Parameter segment_unambiguity : (* TODO naming cf LEM *)
  forall a res s,
    d.segment_of_action_with_res a res s ->
    (forall a' res',
       a <> a' -> ~d.segment_of_action_with_res a' res' s) /\
    (forall res',
       res <> res' -> ~d.segment_of_action_with_res a res' s).

(** Only a full segment should qualify as a [segment_of_action_with_res] *)
(* TODO feels too strong a requirement: e.g. repeat msg till success succ::fail::...::fail *)
Parameter segment_entirety : (* TODO naming *)
  forall a res s,
    d.segment_of_action_with_res a res s ->
    (forall s', proper_sublist_of s' s -> forall a' res', ~d.segment_of_action_with_res a' res' s').

Corollary segment_entirety_cor :
  forall a res s,
    d.segment_of_action_with_res a res s ->
    (forall s', proper_sublist_of s s' -> forall a' res', ~d.segment_of_action_with_res a' res' s').
Proof.
  intros. intro. exact (segment_entirety _ _ _ H1 _ H0 _ _ H).
Qed.

Definition action_res_eq (a1 a2 : d.action) (res1 : d.action_res a1) (res2 : d.action_res a2) : Prop.
Proof.
  destruct (d.eq_action_dec a1 a2); [ | exact False ].
  subst a2. exact (res1 = res2).
Defined.

(** Part of one segment cannot be part of a different segment *)
Parameter segment_no_recombine_before : (* TODO naming *)
  forall a res s,
    d.segment_of_action_with_res a res s ->
    forall p,
    proper_prefix_of p s ->
    forall a' res' s', ~d.segment_of_action_with_res a' res' (s' ++ p).

Parameter segment_no_recombine_after : (* TODO naming *)
  forall a res s,
    d.segment_of_action_with_res a res s ->
    forall p,
    proper_postfix_of p s ->
    forall a' res' s', ~d.segment_of_action_with_res a' res' (p ++ s').

Lemma overlapping_segments :
  forall a1 res1 s1
         a2 res2 s2,
    d.segment_of_action_with_res a1 res1 s1 ->
    d.segment_of_action_with_res a2 res2 s2 ->
    proper_sublist_of s1 s2 ->
    False.
Proof.
  intros.
  assert (tmp1 := segment_entirety _ _ _ H0).
  assert (tmp2 := tmp1 _ H1). clear tmp1.
  exact (tmp2 _ _ H).
Qed.

End DomainSegmentCanonicalProperties.
