Require Import result.
Require Import dec_utils.

Require Import IO.intf.World.
Require Import IO.intf.IORaw.

Require Import ElemHandle.
Require Import ElemDomain.

(** * Actions for a domain containing instances a different domain. *)
Module Type DomainArrayActions
       (w : World)
       (ior : IORaw0 w)
       (subid : ElemHandle)
       (subd : ElemDomain w ior subid)
.

(** The name identifying a subdomain instance. *)
Parameter name :
  Set.
Parameter eq_name_dec :
  forall n1 n2 : name, {n1=n2}+{n1<>n2}.

(*************** Basic [Domain] data *******************)
(** The actions this domain defines. *)
Inductive dw_action :
  Set :=
| dw_action_open : name -> dw_action
| dw_action_close : subid.handle -> dw_action
| dw_action_operate : subd.action -> dw_action.
Definition action := dw_action.

Definition eq_action_dec :
  forall a1 a2 : action,
    {a1 = a2}+{a1 <> a2}.
Proof.
  assert (ne := eq_name_dec).
  assert (he := subid.eq_handle_dec).
  assert (ae := subd.eq_action_dec).
  decide equality.
Defined.

(** The type of errors produced while operating in the array domain *)
Parameter error_code :
  Set.
Parameter eq_error_code_dec :
  forall ec1 ec2 : error_code, {ec1=ec2}+{ec1<>ec2}.

(** Specify the type of the result when [a] is executed *)
Definition action_res
           (a : action) :
  Set :=
  match a with
    | dw_action_open _ => result subid.handle error_code
    | dw_action_close _ => result unit error_code
    | dw_action_operate sa => subd.action_res sa
  end.

Definition eq_action_res_dec :
  forall (a : action) (res1 res2 : action_res a),
    {res1 = res2}+{res1 <> res2}.
Proof.
  assert (ne := eq_name_dec).
  assert (he := subid.eq_handle_dec).
  assert (ee := eq_error_code_dec).
  assert (ae := subd.eq_action_dec).
  assert (are := subd.eq_action_res_dec).
  assert (ue := eq_unit_dec).
  destruct a; intros; try decide equality.
  apply are.
Defined.

End DomainArrayActions.
