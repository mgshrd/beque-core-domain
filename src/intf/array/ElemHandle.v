Module Type ElemHandle
.

(** A descriptor for an opened subdomain instance. *)
Parameter handle :
  Set.

Parameter eq_handle_dec :
  forall h1 h2 : handle, {h1=h2}+{h1<>h2}.

End ElemHandle.
