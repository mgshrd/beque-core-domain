Require Import sublist.

Require Import IO.intf.World.
Require Import IO.intf.IORaw.
Require Import IO.intf.IOAux.

Require Import LTL.util.LTL.

Require Import Domain.

Require Import ElemHandle.


Module Type ElemDomain
       (w : World)
       (ior : IORaw0 w)
       (id : ElemHandle)
<: Domain w ior
.

Module io := IOAux0 w ior.
Module ltl := LTL w.

#import action
Parameter action_for :
  action -> id.handle.

#import eq_action_dec
#import action_res
#import eq_action_res_dec
#import prop
Parameter prop_for :
  prop -> id.handle.

#importrest

End ElemDomain.
